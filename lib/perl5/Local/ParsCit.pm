package Local::ParsCit;

###
# this is an adaption of the original - to adjust for language differences
###
use ParsCit::PreProcess;
*ParsCit::PreProcess::FindHeaderText = sub
{
    my ($lines, $start_id, $num_lines) = @_;

    if($start_id >= $num_lines) { die "Die in SectLabel::PreProcess::findHeaderText: start id $start_id >= num lines $num_lines\n"; }

    my $body_start_id = $start_id;
    for(; $body_start_id < $num_lines; $body_start_id++)
    {
        if($lines->[$body_start_id] =~ /^(.*?)\b(Introduzione|Abstract|ABSTRACT|Introductions?|INTRODUCTIONS?)\b(.*?):?\s*$/) 
        {
            # There are trailing text after the word introduction
            if (CountTokens($3) > 0)
            {
                # INTRODUCTION AND BACKGROUND
                if($3 =~ /background/i) { last; }
            } 
            else 
            {
                last;
            }
        }
    }

    my $header_length       = $body_start_id - $start_id;
    my $body_length         = $num_lines - $body_start_id;

    if ($header_length >= 0.8*$body_length) 
    {
        print STDERR "Header text $header_length longer than 80% article body length $body_length: ignoring\n";

        $body_start_id  = $start_id;
        $header_length  = 0;
        $body_length    = $num_lines - $body_start_id;
    }

    if ($header_length == 0) { print STDERR "warning: no header text found\n"; }

    return ($header_length, $body_length, $body_start_id);
}; 

*ParsCit::PreProcess::FindCitationText = sub
{
    my ($rtext, $pos_array) = @_;

	# Save the text
	my $text		= $$rtext;
    my $bodytext	= "";
    my $citetext	= "";

	###
	# Corrected by Cheong Chi Hong <chcheong@cse.cuhk.edu.hk> 2 Feb 2010
	# while ($text =~ m/\b(References?|REFERENCES?|Bibliography|BIBLIOGRAPHY|References?\s+and\s+Notes?|References?\s+Cited|REFERENCE?\s+CITED|REFERENCES?\s+AND\s+NOTES?):?\s*\n+/sg) 
	# {
	###
	###
	# Corrected by Huy Do, 15 Jan 2011
    # while ($text =~ m/\b(References?|REFERENCES?|Bibliography|BIBLIOGRAPHY|References?\s+and\s+Notes?|References?\s+Cited|REFERENCES?\s+CITED|REFERENCES?\s+AND\s+NOTES?):?\s*\n+/sg)
	# {
	###
    while ($text =~ m/\b(Bibliografia|BIBLIOGRAFIA(\s+CONSIGLIATA)?|bibliografia|References?|REFERENCES?|Bibliography|BIBLIOGRAPHY|References?\s+and\s+Notes?|References?\s+Cited|REFERENCES?\s+CITED|REFERENCES?\s+AND\s+NOTES?|LITERATURE?\s+CITED?):?\s*\n+/sg) 
	{
		$bodytext = substr $text, 0, pos $text;
		$citetext = substr $text, pos $text unless (pos $text < 1);
    }

	# No citation
	if ($citetext eq "")
	{
		print STDERR "Citation text cannot be found: ignoring", "\n";
		return \$citetext, ParsCit::PreProcess::NormalizeBodyText(\$bodytext, $pos_array), \$bodytext;
	}

	# Odd case: when citation is longer than the content itself, what should we do?
    if (length($citetext) >= 0.8 * length($bodytext)) 
	{
		print STDERR "Citation text longer than article body: ignoring\n";
		return \$citetext, ParsCit::PreProcess::NormalizeBodyText(\$bodytext, $pos_array), \$bodytext;
    }

	# Citation stops when another section starts
    my ($scitetext, $tmp) = split(/^([\s\d\.]+)?(Appendice|Acknowledge?ments?|Autobiographical|Tables?|Appendix|Exhibit|Annex|Fig|Notes?)(.*?)\n+/m, $citetext);

	if (length($scitetext) > 0) { $citetext = $scitetext; }

	# No citation exists
    if ($citetext eq '0' || ! defined $citetext) { print STDERR "warning: no citation text found\n"; }

	# Now we have the citation text
	return (ParsCit::PreProcess::NormalizeCiteText(\$citetext), ParsCit::PreProcess::NormalizeBodyText(\$bodytext, $pos_array), \$bodytext);
};
