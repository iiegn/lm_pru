:orphan:

.. _subindex:

LM/PRU
======

.. include:: ../_blocks/summary.txt 

The project's source code is available from |url_source|, and its documentation
from |url_doc|. A RESTful interface is located at |url_service|, and an example
use-case of the RESTful interface (upload file, run the pipeline, make results
available for download) is available at |url_test|.

.. toctree::
    :maxdepth: 2
    
    pipeline
    api
    modules

.. |url_source|     replace::
    .. include:: ../_blocks/url_source

.. |url_doc|        replace:: 
    .. include:: ../_blocks/url_doc

.. |url_service|    replace:: 
    .. include:: ../_blocks/url_service

.. |url_test|       replace::
    .. include:: ../_blocks/url_test
