:tocdepth: 3

API
===

.. include:: ../manual/api.rst


Examples
--------

.. include:: ../manual/api_examples.rst
