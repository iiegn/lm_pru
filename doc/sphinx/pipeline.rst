Pipeline
========

.. include:: ../../src/bin/pipeline.sh
    :start-after: #"""
    :end-before: """
    :literal:

.. include:: ../manual/pipeline.rst
