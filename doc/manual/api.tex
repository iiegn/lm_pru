The LM/PRU pipeline is accessable via an Application Programming Interface (API).
The interface uses a RESTful protocol on top of the Hypertext Transfer Protocol
(HTTP) version 1.1; clients submit source documents via POST requests, access
results via GET requests, and request deletion of resources via DELETE
requests.
Results are either Extensible Markup Language (XML) documents, or documents of
the requested type.

Requests for running the pipeline on new source documents are handled
independently, and a new resource is created per request, i.e.~only one
document can be submitted at a time but multiple parallel requests are possible
as well as, parallel processing of multiple source documents on the server.
% if we make sure the created URI is 'truly' random, and we switch from http to
% https, there is also a lot less need for introducing access control levels
% (ACLs). 
% FIXME: introduce securely transmitted random ID

A typical client-server interaction looks like this:
Upon successful POST of a new source document by the client, the server
responds with a new resource URI. The client uses this URI to poll the status
of the processing until successful completion is being signalled. Now, the
client can retrieve the final output (in anyone of the available formats).
 
\subsection{Protocol}
The REST architectural style implies that URLs are used to represent available
resources on a server. 
This section gives a summary of the resources that are available from the
PRU/LM interface with the HTTP-methods that can be used on them. 

\subsubsection{Available Resources and Supported Methods}
The \verb+<BASE_URL>+ denotes the location on a server where the LM/PRU
pipeline's interface is exposed on a web server, e.g.
\url{http://clic.cimec.unitn.it/egon/pru}. 
Likewise, \verb+<ID>+ denotes a specific resource returned by the server upon
successful POST of a source document.

\begin{figure*}
%\lstset{}
%\begin{lstlisting}
\begin{verbatim}
<BASE_URL>
    /documents            -- POST   : creation of new resource
        /<ID>             -- DELETE : deletion of resource
            /status       -- GET    : status of backend processing
            /log          -- GET    : log file of backen processing
            /pdf          -- GET    : previously uploaded PDF document
            /bib          -- GET    : extracted references in BibTeX format
            /pretxp       -- GET    : structure-annotated pre-TextPro file
            /txp          -- GET    : TextPro processed file
            /zip          -- GET    : packed directory content
            /
\end{verbatim}
%\end{lstlisting}
\end{figure*}

\subsubsection{Expected and Available Formats}

\paragraph{/documents}
To request the creation of a new resource the client calls this URI via HTTP
POST using the Content-Type multipart/form-data according to RFC2388. A single
file with the parameter NAME and the VALUE "upfile" will be considered.

An exemplary html form:
\lstset{language=XML}
\begin{lstlisting}
<form method="POST" enctype="multipart/form-data" ACTION="./documents">
   <input type="file" name="upfile" maxlength="1" />
   <input type="submit" id="sendForm" value="upload" />
</form>
\end{lstlisting}

\paragraph{/documents/<ID>}
To request the deletion of the resource, i.e.~the whole processed directory,
the client calls this URI via HTTP DELETE.

\paragraph{/documents/<ID>/status}
To request the state of the processing the client calls this URI via HTTP GET. 
A single text/plain document with the status will be returned.

\paragraph{/documents/<ID>/log}
To request the log file of the processing the client calls this URI via HTTP
GET. 
A single text/plain document with the log file as content will be
returned.

\paragraph{/documents/<ID>/pdf}
To request the original, unmodified file the client calls this URI via HTTP
GET. 
The response will be of "Content-Type:application/pdf", the actual content will
be binary data. Calling this URI from a browser will (usually)
trigger a 'Save File' dialogue.

\paragraph{/documents/<ID>/bib}
To request the extracted references in BibTeX format the client calls this URI
via HTTP GET. 
A single text/plain document will be returned.

\paragraph{/documents/<ID>/pretxp}
To request the pre-TextPro file the client calls this URI via HTTP GET. 
A single text/plain document will be returned.

\paragraph{/documents/<ID>/txp}
To request the TextPro processed file the client calls this URI via HTTP GET. 
A single text/plain document will be returned.

\paragraph{/documents/<ID>/zip}
To request the gzip compressed tar archive of the processed directory the client
calls this URI via HTTP GET. The response will be of
"Content-Type:application/x-gzip", and will have a "Content-Disposition:
attachment", the actual content will be binary data. Calling this URI from a
browser will (usually) trigger a 'Save File' dialogue.

\subsubsection{Processing Status}
The client may query the latest status of the processing via HTTP GET on the
status URI.

\begin{figure*}
\begin{verbatim}
The status responses are:
 Q:queued    -- upon successful resource creation, no processing has happened
 R:running   -- document is being processed, continuously growing log file
                available
 S:succeeded -- document has been processed successfully, results available
 F:failed    -- document processing has failed, log file available
\end{verbatim}
\end{figure*}

While the status codes 'Q' and 'R' are intermediate and changes can be
identified via re-polling, the status code 'S' and 'F' are final, i.e.~no
status change will occur, and clients must not continue polling.
