# Makefile to take care of work inside sub directories 
#
SUBDIRS := doc
all: doc
default: doc
.PHONY: all default

$(SUBDIRS)::
	$(MAKE) $(MFLAGS) -C $@ $(MAKECMDGOALS)

.PHONY: subdoc
subdoc: 
	$(MAKE) $(MFLAGS) -C doc subdoc

.PHONY: clean
clean : $(SUBDIRS)
