#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s < text

Guess the langueage of text on standard input. 
"""

from os.path import basename
from sys import argv, exit, stdin, stderr
import re
import cld

def guess_lang(text):
    """
    Return the guessed language for text; or "Unknown" if guess is shaky, to
    text.split() yields only few tokens.

    Arguments:
        text        -- utf-8 encoded plain text 
    """
    ret_val = "Unknown"
    lang_gloss, lang_id, confident, _, _ = cld.detect(text, 
            isPlainText=True, includeExtendedLanguages=False)
    if (confident is True and
            lang_gloss != "Ignore" and
            lang_gloss != "Unknown"):
        ret_val = lang_gloss
    return ret_val

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

if __name__ == "__main__":
    if len(argv) > 1:
        print >> stderr, usage() 
        exit(1)

    lines = []
    for lnum, line in enumerate(stdin.xreadlines()):
        line = line.strip()
        lines.append(line)

    print guess_lang(" ".join(lines))
    #print cld.detect(" ".join(lines), isPlainText=True, 
    #        includeExtendedLanguages=False)
