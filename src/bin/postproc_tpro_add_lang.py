#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s [DEFAULT_LANG] < TextPro_file

    DEFAULT_LANG            -- the default language to use when id fails
    TextPro_file            -- TextPro file read from STDIN

post-process textpro output: add '# LANGUAGE:' comment to each block.
"""
from os.path import basename
from sys import argv, exit, stderr, stdin

from langid import guess_lang
from lib_textpro import (get_comment_value, get_content_col,
                         get_fields_col_number, set_comment, tp_blocks,
                         LANGUAGE_MAPPINGS_DICT)

def add_lang(comment, guess_text, language_default=None):
    """
    Guess the language of guess_text, add a TextPro '# LANGUAGE:' comment, and
    return the new comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        guess_text  -- string of which the lang is to be guessed
        language_default -- the default if guessing fails
    """
    ret_lang = None 
    tpro_lang = get_comment_value(comment, "LANGUAGE")
    section = get_comment_value(comment, "SECTION")
    
    if not tpro_lang:
        guessed_lang = guess_lang(guess_text)
        for lang_map in LANGUAGE_MAPPINGS_DICT:
            if section and section.lower().startswith(lang_map):
                ret_lang = LANGUAGE_MAPPINGS_DICT[lang_map]
            elif (guessed_lang.lower() == lang_map):
                ret_lang = LANGUAGE_MAPPINGS_DICT[lang_map]
        if not ret_lang and language_default:
            ret_lang = language_default
    else:
        ret_lang = tpro_lang

    if ret_lang:
        comment = set_comment(comment, "LANGUAGE", ret_lang)
    return comment
    
def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

if __name__ == "__main__":
    if len(argv) > 2:
        print >> stderr, usage() 
        exit(1)
    elif len(argv) == 2:
        LANGUAGE_DEFAULT = argv[1]
    else:
        LANGUAGE_DEFAULT = None

    for COMMENT, CONTENT in tp_blocks(stdin):
        TOKENS = get_content_col(CONTENT, get_fields_col_number(COMMENT,
            "token"))
        LEMMATA = get_content_col(CONTENT, get_fields_col_number(COMMENT,
            "lemma")) 
        GUESS_TEXT = " ".join(TOKENS).lower()
        print "\n".join(add_lang(COMMENT, GUESS_TEXT, 
            language_default=LANGUAGE_DEFAULT))
        print "\n".join(CONTENT)
        print
