#!/bin/bash

BN=$(basename "$0")
usage()
{
    cat >&2 << ---SNIP---
Usage: ${BN} MODEL ParsCit-ROOT

 MODEL           select the model to switch to
                 possible values: 
                     dist - original model from the distribution
                     it   - 'our' trained model for Italian

 ParsCit-ROOT    the root directory of the ParsCit installation

A run-time configuration hack for ParsCit - make different language models
available without to much ado elsewhere.
---SNIP---
}

if [ -z "$1" ] || [ -z "$2" ]
then
    usage
    exit 1
fi

MODEL="$(tr [A-Z] [a-z] <<< "$1")"
PARSCIT="$2"

# we will hack by creating syslinks for (some of):
#
## ParsCit::Config
# dictFile          = "resources/parsCitDict.txt";
# crf_test          = "crfpp/crf_test";
# modelFile         = "resources/parsCit.model";
# splitModelFile    = "resources/parsCit.split.model";
#
## ParsCit::Tr2crfpp
# dict_file         = "$FindBin::Bin/../resources/parsCitDict.txt";
# crf_test          = "$FindBin::Bin/../crfpp/crf_test";
# model_file        = "$FindBin::Bin/../resources/parsCit.IT.model";
# split_model_file  = "$FindBin::Bin/../resources/parsCit.split.model";

make_links()
{
    [ -z $1 ] && exit 1
    if [ ! -e "${PARSCIT}/resources/parsCit.model.$1" ] \
    || [ ! -e "${PARSCIT}/resources/parsHed/parsHed.model.$1" ] \
    || [ ! -e "${PARSCIT}/resources/sectLabel/sectLabel.model.$1" ]
    then
        echo >&2 "${BN}: non-existing file as target for a symlink - jfyi."
    fi 
    ln -sf "parsCit.model.$1" -T "${PARSCIT}/resources/parsCit.model"
    ln -sf "parsHed.model.$1" -T "${PARSCIT}/resources/parsHed/parsHed.model"
    ln -sf "sectLabel.model.$1" -T "${PARSCIT}/resources/sectLabel/sectLabel.model"
}

if [ ! -h "${PARSCIT}/resources/parsCit.model" ] \
|| [ ! -h "${PARSCIT}/resources/parsHed/parsHed.model" ] \
|| [ ! -h "${PARSCIT}/resources/sectLabel/sectLabel.model" ]
then
    echo >&2 "${BN}: pls. make the initial symlinks in '${PARSCIT}'"
    exit 1;
else
    case "${MODEL}" in
        dist)
            make_links "dist"
            ;;
        it|en)
            make_links "it"
            ;;
        foo)
            make_links "foo"
            ;;
        *)
            echo >&2 "${BN}: unknown MODEL:${MODEL}."
            exit 1
            ;;
    esac
fi

exit 0
