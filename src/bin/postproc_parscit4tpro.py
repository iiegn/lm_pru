#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s [DEFAULT_LANG] < $(cat PARSCIT.xml.head PARSCIT.xml)

post-process a con`cat`enated ./ParsCit -m extract_header and 
ParsCit -m extract_all file for TextPro to standard output:

split the input:
await line "^<algorithm name="ParsHed"').*$" first, and then, 
await line "^<algorithm name="SectLabel"').*$"; then, extract header info from
the former mini-dom, and section info from the latter. 

extract header and section info:
Use the positive HEADER_CONTENT_TAGS_LIST with HEADER_CONTENT_CONFIDENCE, and 
the negative SECTION_CONTENT_NO_TAGS_LIST with SECTION_CONTENT_CONFIDENCE to
 - extract header info if ParsCit confidence is high enough
 - discard section info if ParsCit confidence 

add LANGUAGE info for TextPro:
 - try to guess lang, use DEFAULT_LANG if this fails
""" 
import difflib
from os.path import basename
from sys import argv, exit, stdin, stderr
from xml.dom.minidom import parseString

from postproc_tpro_add_lang import add_lang

HEADER_CONTENT_TAGS_LIST = (["title", "author", "email", "affiliation",
    "volumeinfo", "volumeInfo", "abstract", "englishabstract", "italianabstrct", 
    "summary", "englishsummary", "italiansummary", 
    "keywords", "englishkeywords", "italiankeywords", 
    "note"])
HEADER_CONTENT_CONFIDENCE = 0.75

SECTION_CONTENT_NO_TAGS_LIST = (["annex", "apendix", "construct", 
    "englishfigurecaption", "englishtablecaption", 
    "equation", "figure", "figureCaption",
    "italianfigurecaption", "italiantablecaption", "listItem", "page", 
    "reference", "references", "table"])
SECTION_CONTENT_CONFIDENCE = 0.5

def get_text(node):
    """ Return u'TEXT' from node, i.e. its sibling. """
    text = []
    if node.hasChildNodes:
        for node in node.childNodes:
            if node.nodeType == node.TEXT_NODE:
                text.append(node.data)
    return "".join(text).encode("utf-8")

def get_header_content(dom, tag_list, min_conf_val=0.75):
    """ 
    Return tuple (list of content, non_content lines); use tag_list to find
    elements in dom and add their text data to content if the respective 
    confidence is higher than min_conf_val; otherwise add the text data to
    non_content.
    """
    content = []
    non_content = []
    for tag in tag_list:
        for node in dom.getElementsByTagName(tag):
            conf = node.getAttribute("confidence")
            text = get_text(node)
            try:
                conf = float(conf)
                if conf >= min_conf_val and text.strip() != "":
                    content.append((tag, str(conf), text))
                elif text.strip() != "":
                    non_content.append((tag, str(conf), text))
            except ValueError:
                pass
    return content, non_content

def get_section_content(node, exclude_tags_list, min_conf_val=0.65):
    """ 
    Return list of content lines; find elements in node and its siblings and
    discard those from exclude_tags_list; if confidence higher than 
    min_conf_val, then use the nodes tag for TextPro output.
    """
    # FIXME: look into what-do-we-discard-here; currently, exclude_tags_list
    # is used, and corresponding nodes are dicarded - independetly of their
    # confidence.
    content = []
    if node.nodeName not in exclude_tags_list and node.hasChildNodes():
        try:
            conf = node.getAttribute("confidence")
            conf = float(conf)
            text = get_text(node)
            if conf >= min_conf_val and text.strip() != "":
                content.append((node.nodeName, str(conf), text))
            elif text.strip() != "":
                content.append(("", str(conf), text))
        except ValueError:
            pass
        except AttributeError:
            pass
        for cn in node.childNodes:
            content = content + get_section_content(cn, exclude_tags_list,
                    min_conf_val)
    return content

def print_for_textpro(content):
    """ 
    Print content to stdout; prepare it for TextPro, i.e. make it look:
     --- 8< ---
     # FILE: 
     # SECTION: SOME_TAG
     # LANGUAGE:
     # FIELDS: 
     one paragraph per line.
     <LF> 
     # FILE:
     ...
     <LF>
     --- 8< ---
    """
    for tag, conf, text in content:
        hashtag_lines = []
        if tag != "":
            hashtag_lines.append("# SECTION: "+tag.upper())
            # hashtag_lines.append("# CONFIDENCE: "+conf)

        lines = text.split("\n")
        for lnum, line in enumerate(lines):
            comment_list = []
            if line.strip() != "":
                comment_list.append("# FILE: 001")
                # FIXME: ask consumers whether they needs continuation tag
                #if lnum > 1:
                #    comment_list.append("# I-SECTION")
                if len(hashtag_lines) > 0:
                    comment_list += hashtag_lines
                comment_list.append("# FIELDS:")

                guess_text = line.lower()
                comment = add_lang(comment_list, line, 
                        language_default=LANGUAGE_DEFAULT)

                print "\n".join(comment)
                print line
                print

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

if __name__ == "__main__":
    if len(argv) > 2:
        print >> stderr, usage() 
        exit(1)
    elif len(argv) == 2:
        LANGUAGE_DEFAULT = argv[1]
    else:
        LANGUAGE_DEFAULT = None

    header = []
    section = []
    in_header = in_section = False

    for LINE in stdin.xreadlines():
        if (LINE.startswith('<algorithm name="ParsHed"') and 
                len(header) == 0):
            header.append("")
            in_header = True
        elif (LINE.startswith('<algorithm name="SectLabel"') and 
                len(section) == 0):
            section.append("")
            in_section = True
        elif ((in_header or in_section) and 
                LINE.startswith('</algorithm>')):
            if in_header:
                header.append(LINE)
            elif in_section: 
                section.append(LINE)
            in_header = in_section = False

        if in_header:
            header.append(LINE)
        elif in_section:
            section.append(LINE)

    DOM = parseString("".join(header))
    header_content, non_header_content = get_header_content(DOM,
            HEADER_CONTENT_TAGS_LIST, HEADER_CONTENT_CONFIDENCE)

    DOM = parseString("".join(section))
    section_content = get_section_content(DOM, HEADER_CONTENT_TAGS_LIST +
        SECTION_CONTENT_NO_TAGS_LIST, SECTION_CONTENT_CONFIDENCE)

    def similar(seq1, seq2):
        return (difflib.SequenceMatcher(a=seq1.lower(), b=seq2.lower()).ratio()
                > 0.9)
    
    # try to purge dupicates from header and content section
    # these may happen because the header and the section are processed by
    # different algorithms, and hence, might het assigned different tags.
    for _,_,h_con in header_content:
        for s_con_idx, (_,_,s_con) in enumerate(section_content):
            if similar(" ".join(h_con), " ".join(s_con)):
                section_content.pop(s_con_idx)

    print_for_textpro(header_content)
    print_for_textpro(section_content)
