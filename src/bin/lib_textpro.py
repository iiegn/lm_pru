# -*- coding: utf-8 -*-
"""
commonly needed functionality for dealing with TextPro files.
"""

## 
# constants we will use while dealing whith TextPro files
#
# TextPro columns (as we use them...)
# FIELDS: token	tokenstart	sentence	pos	lemma	entity  chunk   nerType
#
TEXTPRO_COL_DEFAULT_VAL = "O"
LANGUAGE_MAPPINGS_DICT = {"english":"ENG", "italian":"ITA"}

def find_patterns(patterns, tokens):
    """
    For each list of tokens in patterns find the longest, exact matching
    sequences within the list of tokens, and 
    return a dictionary built of tuples (token_start_id, length_of_match).

    Arguments:
        patterns    -- list of lists of tokens
        tokens      -- list of tokens
    """
    start_tok_ids = {}
    tokens_dict = {}
    for tok_id, token in enumerate(tokens):
        tokens_dict[token] = tokens_dict.get(token, list()) + [tok_id]

    for _, pattern in enumerate(patterns):
        for tok_id in [tok_ids for tok_ids in tokens_dict.get(pattern[0], [])]:
            if pattern == tokens[tok_id:tok_id+len(pattern)]:
                old_pattern_len = start_tok_ids.setdefault(tok_id, len(pattern))
                if len(pattern) > old_pattern_len:
                    start_tok_ids[tok_id] = len(pattern)
    
    return start_tok_ids.items()

def tp_blocks(stream):
    """
    Return tuple: list of comment lines, and list of content lines.

    Arguments:
        stream      -- a 'TextPro file' input stream (e.g. sys.stdin)
    """
    def _split_tp_block(block):
        """ 
        Return the tuple of comment and content lines. 
        
        Arguments:
            block   -- list of lines of a TextPro block
        """
        comment = []
        content = []
        for line in [line.strip() for line in block]:
            if line.startswith("# "):
                comment.append(line)
            elif len(comment) > 0:
                content.append(line)
            else:
                # -> comment after content - strange!
                comment = []
                content = []
                break
        return comment, content

    lines_buffy = [] 
    while True:
        line = stream.readline()
        if line == "":
            if len(lines_buffy) > 0:
                yield _split_tp_block(lines_buffy)
            lines_buffy = []
            break
        elif line == "\n" and len(lines_buffy) > 0:
            yield _split_tp_block(lines_buffy)
            lines_buffy = []
        elif line == "\n":
            pass
        else:
            lines_buffy.append(line.strip())

def get_fields_col_number(comment, col_label):
    """
    Return col_label's index number of the "# FIELDS:" line of the comment
    block.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label   -- string: name of the column
    """
    try:
        fields = comment[-1][len("# FIELDS: "):].split("\t")
        retval = fields.index(col_label)
    except ValueError:
        retval = None
    finally: 
        return retval

def add_fields_col(comment, col_label):
    """
    Add a column col_label at the end to the "# FIELDS:" line of the comment
    block, and return the altered comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label   -- string: name of the column
    """
    if comment[-1].startswith("# FIELDS:"):
        comment[-1] = "\t".join([comment[-1], col_label])
    return comment

def del_fields_col_by_int(comment, col_num):
    """
    Delete col_num column from the "# FIELDS:" line of the comment block, and
    return the altered comment. 

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_num     -- int of the column to delete
    """
    if comment[-1].startswith("# FIELDS:"):
        fields = comment[-1][len("FIELDS: ")+1:].split("\t")
        fields.pop(col_num)
        comment[-1] = "# FIELDS: " + "\t".join(fields)
    return comment

def del_fields_col_by_name(comment, col_label):
    """
    Delete col_label column from the "# FIELDS:" line of the comment block, and
    return the altered comment. 

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_labe    -- string: name of the column to delete
    """
    return del_fields_col_by_int(comment, get_fields_col_number(comment,
        col_label))

def insert_fields_col_after_int(comment, col_label, col_int):
    """
    Insert col_label_new after the col_label_old column of the "# FIELDS:" line
    of the comment, and return the altered comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label_old -- string: new name of the column to insert
        col_int     -- int of the column to insert /after/
    """
    if comment[-1].startswith("# FIELDS:"):
        fields = comment[-1][len("FIELDS: ")+1:].split("\t")
        fields.insert(col_int+1, col_label)
        comment[-1] = "# FIELDS: " + "\t".join(fields)
    return comment

def insert_fields_col_after_name(comment, col_label_new, col_label_old):
    """
    Insert col_label_new after the col_label_old column of the "# FIELDS:" line
    of the comment, and return the altered comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label_new -- string: new name of the column to insert
        col_label_old -- string: name of the column to insert /after/
    """
    return insert_fields_col_after_int(comment, col_label_new,
            get_fields_col_number(comment, col_label_old))
    
def set_fields_col_by_int(comment, col_label, col_num):
    """
    Set the col_num column of the "# FIELDS:" line of the comment block to
    col_label, and return the altered comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label   -- string: name of the column
        col_num     -- int: the column number to set
    """
    comment = del_fields_col_by_int(comment, col_num)
    comment = insert_fields_col_after_int(comment, col_label, col_num-1) 
    return comment

def set_fields_col_by_name(comment, col_label_new, col_label_old):
    """
    Set the col_label_old column of the "# FIELDS:" line of the comment block to
    col_label_new, and return the altered comment.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        col_label_new -- string: new name of the column
        col_label_old -- string: name of the column to set
    """
    return (set_fields_col_by_int(comment, col_label_new,
        get_fields_col_number(comment, col_label_old)))

def _mk_comment_key(key):
    """Construct the key part of a TextPro comment line."""
    return "".join(["# ", key, ":"])

def _mk_comment_line(key, value):
    """Contruct a whole TextPro comment line from the given key and value."""
    return " ".join([_mk_comment_key(key), value])

def _add_comment_line(comment, line):
    """
    Add comment line to the TextPro comment block, and
    return the altered comment.
    """
    if comment[-1].startswith("# FIELDS:"):
        fields_line = comment[-1]
        comment[-1] = line.strip()
        comment.append(fields_line)
    else:
        comment.append(line)
    return comment

def add_comment(comment, key, value):
    """
    Add key-value-line to comment (right before the "# FIELDS:" line), and
    return the altered comment block.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        key         -- string: new key to add
        values      -- string: corresponding values for the key
    """
    return _add_comment_line(comment, _mk_comment_line(key, value))

def del_comments_by_key(comment, key):
    """
    Delete (all) key-value-line(s) from comment, and
    return the altered comment block.
    """
    for line_idx, line in enumerate(comment):
        if line.startswith(_mk_comment_key(key)):
            comment.pop(line_idx)
    return comment

def get_comment_value(comment, key):
    """
    Return the (first) value for the comment key from the comment block.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        key         -- string: the key to look for
    """
    ret_val = None
    for line in comment:
        if line.startswith("".join(["# ", key, ":"])):
            ret_val = line.split(':', 1)[1].strip()
            break
    return ret_val

def set_comment(comment, key, value):
    """
    Set line of comment (re-use old line if available), and
    return the altered comment block.
    
    Arguments:
        comment     -- list of strings: a TextPro comment block
        key         -- string: new key to add
        values      -- string: corresponding values for the key
    """
    if get_comment_value(comment, key):
        del_comments_by_key(comment, key)
    return add_comment(comment, key, value)

def get_content_col(content, col):
    """
    Return the columnn col from the content block.

    Arguments:
        content     -- list of strings: a TextPro content block
        col         -- int: index of the column 
    """
    return [line.split('\t')[col] for line in content]

def insert_content_col_after_col(content, col_lines_list, col_num):
    """
    Insert col_lines as column after col_num in content block, and
    return the altered content.

    Arguments:
        content     -- list of strings: a TextPro content block
        col_lines_list  -- list of strings 
        col_num     -- int of the column to insert /after/
    """
    for line_id, line in enumerate(content):
        parts_list = line.strip().split("\t")
        if col_lines_list[line_id] == "":
            parts_list.insert(col_num+1, TEXTPRO_COL_DEFAULT_VAL)
        else:
            parts_list.insert(col_num+1, col_lines_list[line_id])
        content[line_id] = "\t".join(parts_list)
    return content

def del_content_col(content, col_num):
    """
    Delete col_num column from content block, and
    return the altered content.

    Arguments:
        content     -- list of strings: a TextPro content block
        col_num     -- int of the column to delete
    """
    for line_id, line in enumerate(content):
        parts_list = line.strip().split("\t")
        parts_list.pop(col_num)
        content[line_id] = "\t".join(parts_list)
    return content

def add_content_col(content, col_lines_list):
    """
    Add column at the end of content block, set it to the value of 
    col_lines_list, and return the altered content.

    Arguments:
        content     -- list of strings: a TextPro content block
        col_lines_list  -- list of strings 
    """
    content = insert_content_col_after_col(content, col_lines_list,
            len(content[0].split("\t")))
    return content

def set_content_col(content, col_lines_list, col_num):
    """
    Set column col_num in content block to the values of col_lines_list, and
    return the altered content.
    Raises IndexError if col_num is out of range.

    Arguments:
        content     -- list of strings: a TextPro content block
        col_lines_list  -- list of strings 
        col_num     -- int
    """
    content = del_content_col(content, col_num)
    content = insert_content_col_after_col(content, col_lines_list, col_num-1)
    return content

def add_col(comment, content, col_label, col_lines_list, col_num=None):
    """
    Set or add column col_label/col_num in comment and content blocks, and
    return the altered comment and content.

    Arguments:
        comment     -- list of strings: a TextPro comment block
        content     -- list of strings: a TextPro content block
        col_label   -- string: name of the column
        col_lines_list  -- list of strings 

        col_num     -- int of the column to set 
    """
    if get_fields_col_number(comment, col_label):
        col_num = get_fields_col_number(comment, col_label)

    if col_num:
        comment = set_fields_col_by_int(comment, col_label, col_num)
        content = set_content_col(content, col_lines_list, col_num)
    else:
        comment = add_fields_col(comment, col_label)
        content = add_content_col(content, col_lines_list) 

    return comment, content
