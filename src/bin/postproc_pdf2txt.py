#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s < pdf2text_FILE 

post-process pdf2txt file to standard out: try to combine lines to form a 
paragraph. 
(cf. code comments for details on rules.)
"""

from os.path import basename
from sys import argv, exit, stdin, stderr
import re

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

if len(argv) > 1:
    print >> stderr, usage() 
    exit(1)

# end-of-line end-of-sentence characters
EOFL_EOS = re.compile("[?).:;]$")
# end-of-line non-end-of-sentence characters
EOFL_CHAR = re.compile("""[&,”"°ùéèà\w]$""", re.UNICODE+re.LOCALE)
# end-of-line non-end-of-sentence sequences 
EOFL_SEQS = re.compile("""a\.\ ?C\.|i\.?e\.|e\.?g\.$""", re.UNICODE+re.LOCALE)
# beginning-of-line sentence continuation characters
BOFL_CHAR = re.compile("""^[(è“"\w]""", re.UNICODE+re.LOCALE)
# end-of-line hyphenated word characters
EOFL_HYPH = re.compile("[ùéèà\w]-$", re.UNICODE+re.LOCALE)

lastlines = ""
len_lastline = 0

for lnum, line in enumerate(stdin.xreadlines()):
    line = line.strip()

    # don't combine lines if last line contained/cosisted of UPPERS
    if line.isupper():
        print lastlines
        lastlines = line

    # don't combine lines if last line was 'short' and last line ended with
    # end-of-sentence char OR
    # last line was /really/ 'short';
    # we assume this was a paragraph ending.
    elif (((len_lastline < 0.75*len(line)) and EOFL_EOS.search(lastlines)) or
            (len_lastline < 0.50*len(line))):
        print lastlines
        lastlines = line

    # combine successive text lines if:
    #   - last line ended with end-of-sentence' char AND
    #     current line has the same length - this is likely a continuation
    elif (EOFL_EOS.search(lastlines) and (len_lastline > 0.90 * len(line)) and 
            (len(line) > 0)):
        lastlines = "".join([lastlines, " ", line])

    # combine successive text lines if:
    #     - last line ended with 'non-end-of-sentence' char and
    #       current line starts with a 'sentence continuation' char
    elif ((EOFL_CHAR.search(lastlines) or EOFL_SEQS.search(lastlines)) and 
            BOFL_CHAR.search(line)):
        lastlines = "".join([lastlines, " ", line])
    
    # combine successive text lines and remove hyphen if:
    #     - last line ended with a letter+'-' combination and
    #       current line starts with a 'sentence continuation' char
    elif EOFL_HYPH.search(lastlines) and BOFL_CHAR.search(line):
        lastlines = "".join([lastlines[:-1], line])
    
    # avoid empty line at the beginning of the output
    elif lnum > 0:
        print lastlines
        lastlines = line
    else:
        lastlines = line

    len_lastline = len(line)
print lastlines
