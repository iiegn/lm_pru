#!/bin/bash

BN=$(basename "$0")
usage()
{
    cat >&2 << ---SNIP---
Usage: ${BN} FILENAME ParsCit-ROOT

 FILENAME        pre-processed file (FILENAME.txt and ~.txt-NUM must exists)
 
 ParsCit-ROOT    the root directory of the ParsCit installation

Run ParsCit on pre-processed data, and produce intermediate data (for further
processing on its way to TextPro&Co.). 
---SNIP---
}

if [ -z "$1" ] || [ -z "$2" ]
then
    usage
    exit 1
fi

FILENAME="$1"
PARSCIT="$2"

shopt -s extglob
perl -I${PRU}/lib/perl5 -I${PRU}/external/perl/lib/perl5 \
     -Mlocal::lib="${PRU}/external/perl" \
     "${PRU}/external/ParsCit/bin/citeExtract.pl" \
     -m extract_header -i raw \
     <( cat "${FILENAME}.txt"-?(000)?(00)?(0)1 ) "$1".xml.head 2>&2
shopt -u extglob

perl -I${PRU}/lib/perl5 -I${PRU}/external/perl/lib/perl5 \
     -Mlocal::lib="${PRU}/external/perl" \
     "${PRU}/external/ParsCit/bin/citeExtract.pl" \
     -m extract_all -i raw -e bib \
     "${FILENAME}".txt "${FILENAME}".xml 2>&2

exit 0
