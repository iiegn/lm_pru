#!/usr/bin/env bash
#"""
# THE pipeline
# handle all processing stages from PDF (with text content, i.e. ready OCR-ed)
# to TextPro output; the latter enriched with 'some additional' information. 
# """

set -o errexit

export PRU=${HOME}/unitn/lm_pru
export TEXTPRO=${PRU}/external/TextPro
[ -d ${PRU}/external/python/bin ] && export PATH=${PRU}/external/python/bin:$PATH
[ -d ${PRU}/external/perl/bin ] && export PATH=${PRU}/external/perl/bin:$PATH

# the file name to process
FN="$1"

echo () {
    /bin/echo $@
    sync
}

# the number of pages in the document at hand
echo "finding pages"
num_pages=$(pdfinfo "$FN" | grep "Pages:" | cut -d':' -f2)
if [ -z "$num_pages" ] || (( "$num_pages" < 1 ))
then 
    echo -e "...didn't find a single page - sorry!\n"
    exit 1
fi
echo -e "...found $num_pages pages.\n"


# extract txt from the individual PDF pages.
# (we will then use only the first page to get some meta information, and
# - knowing about beginning and end of pages - use the individual pages to
# guess headers and footers.)
#
#FIXME: 
# - try to convince pdf2txt to mark 'column jumps' for running text
#   (and use this information during post-processing to combine the text.)
# - try to 'guess/auto-optimize' pdf2txt's values for -L 0.3 -M 1.0 -W 0.2 
# OR
# - use pdftotext with -bbox and use this info to optimize output
echo "converting PDF to txt"
for page in $(seq -w 1 ${num_pages})
do
    #"${PRU}"/external/python/bin/pdf2txt.py -L 0.35 -M 1.15 -W 0.2 -p ${page} "$FN" \
    #> "${FN}.txt-${page}"
    ${PRU}/external/poppler/pdftotext -nopgbrk -f "${page}" -l "${page}" "$FN" "${FN}.txt-${page}"
done
echo -e "...done.\n"
echo "stripping header and footer"
${PRU}/src/bin/strip_headfooter.py "$FN".txt-* \
| ${PRU}/src/bin/postproc_pdf2txt.py > "${FN}.txt"
echo -e "...done.\n"


# guess the (main) language of the document; supposedly, the 'middle' part
# is most reliable hence, guess on this part.
echo "guessing the main language of the document"
if [ "${num_pages}" -gt 2 ]
then
    MAIN_LANG=$(\
    for page in $(seq -w 2 ${num_pages} | head -n -1)
    do
        cat "${FN}.txt-${page}"
    done \
    | "${PRU}"/src/bin/langid.py)
fi
echo "...guessed: ${MAIN_LANG}"

# then, set the appropriate VARs.
echo "setting the LANG vars for ParsCit, TextPro"
LANG_ID=0 # set the default
[ "${MAIN_LANG}" = "ENGLISH" ] && LANG_ID=0
[ "${MAIN_LANG}" = "ITALIAN" ] && LANG_ID=1
PARSCIT_LANG_ARRAY=(en it)
TEXTPRO_LANG_ARRAY=(ENG ITA)
#
PARSCIT_LANG=${PARSCIT_LANG_ARRAY[${LANG_ID}]}
TEXTPRO_LANG=${TEXTPRO_LANG_ARRAY[${LANG_ID}]}
declare -p PARSCIT_LANG
declare -p TEXTPRO_LANG
echo -e "...done.\n"


# extract: 
# - meta-info (like: title, authors, abstract)
# - body text with document structure
# - references
echo "running ParsCit"
"${PRU}"/src/bin/hack_parscit.sh "${PARSCIT_LANG}" "${PRU}/external/ParsCit" 
"${PRU}"/src/bin/run_parscit.sh "$FN" "${PRU}/external/ParsCit"
cat "$FN".xml.head "$FN".xml \
| "${PRU}"/src/bin/postproc_parscit4tpro.py "${TEXTPRO_LANG}" \
> "$FN".pretxp
echo -e "...done.\n"


# Run TextPro - this includes utf8 -> latin1 => TextPro => latin1 -> utf8 
# FIXME: TextPro doesn't like utf-8...
echo "running TextPro"
iconv -f UTF8 -t LATIN1//TRANSLIT//IGNORE "$FN".pretxp > "$FN".pretxp.latin1 \
&& mv "$FN".pretxp.latin1 "$FN".pretxp

[ -e "$FN".pretxp ] \
&& ${TEXTPRO}/textpro.pl -y -l "${TEXTPRO_LANG}" -c token+tokenstart+sentence+pos+lemma+entity -o "$(dirname "$FN")" "$FN".pretxp \
&& mv -v "$FN".pretxp.txp "$FN".txp
iconv -f LATIN1 -t UTF8//TRANSLIT//IGNORE "$FN".txp > "$FN".txp.utf8 \
&& mv "$FN".txp.utf8 "$FN".txp
echo -e "...done.\n"


# Chunk-parse TextPro's output
# note: the extra '\n' is necessary to process the last TP block of a file
echo "ChunkPro-ing TextPro output"
[ -e "$FN".txp ] \
&& "${PRU}"/src/bin/postproc_tpro_add_column.py \
 <(cat "$FN".txp <(echo) | grep -vE "^# " | cut -f1,4 \
  | ${TEXTPRO}/ChunkPro/bin/ChunkPro.sh -m 2 -l "${TEXTPRO_LANG}" \
  | cut -f3):chunk \
 < "$FN".txp \
> "$FN".txp.chn
echo -e "...done.\n"


# Gazetteer
echo "running Gazetteer"
cat "$FN".txp.chn \
| "${PRU}"/src/bin/postproc_tpro_add_gaz.py \
Site:"$PRU"/share/sites.gaz.utf8 Culture:"$PRU"/share/cultures.gaz.utf8 \
> "$FN".txp.chn.gaz
echo -e "...done.\n"

# FIXME:
# asif's pipeline


# Run GeoCoder
# .../after/ ne_tagger because it uses nerType col!
[ -x ${PRU}/external/GeoCoder/geocoder.sh ] \
&& cp "$FN".txp.chn.gaz "$FN".tmp.txp \
&& echo "running GeoCoder" \
&& ${PRU}/external/GeoCoder/geocoder.sh "$FN".tmp.txp "$FN".txp.chn.gaz.geotmp \
&& "${PRU}"/src/bin/postproc_tpro_add_column.py \
 <(sed -e "s/^# .*//" -e "s/\t$/\tO/" "$FN".txp.chn.gaz.geotmp | cut -f9):geoinfo \
 < "$FN".tmp.txp \
> "$FN".txp.chn.gaz.geo \
&& rm "$FN".tmp.txp "$FN".txp.chn.gaz.geotmp \
&& echo -e "...done.\n"

# FIXME:
# fix FILE: header

# .tar.gz the directory
echo "skipping: packing it up"
#tar -czf $(dirname "$FN").tar.gz -C "$(dirname "$FN")/.." "$(basename $(dirname "$FN"))"
echo -e "...done.\n"

exit 0
