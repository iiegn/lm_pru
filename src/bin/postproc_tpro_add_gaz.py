#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s LABEL:FILE... < TextPro_file

    LABEL:FILE              -- LABEL will be the string used for a matched
                                token sequence
                               FILE points to the file containing the patterns,
                                one pattern per line (multi-word tokens
                                allowed)

                               NOTE: the sequence of LABEL:FILE arguments
                               defines the precedence of conflict resolution
                               for conflicting sequence spans

    TextPro_file            -- TextPro file read from STDIN


post-process textpro output: Gazetteer-pimp TextPro's entitiy recognition. 
Use the patterns from the FILE(s), find the longest matching sequences of
lemmata, check that at least one POS tag within the sequence is "SPN", and
introduce a new column "nerType" with I(O)B-notated labels.
In case of conflicting sequence spans use the one from the earlier file.
"""

from os.path import basename
from sys import argv, exit, stderr, stdin

#from lib_textpro import *
from lib_textpro import (add_content_col, add_fields_col,
        get_fields_col_number, find_patterns, get_content_col, tp_blocks)

def gazetteer(patterns_lists, stream):
    """
    Split the stram into TextPro header and content blocks, compare the lemma
    column to the patterns from the list, check at-least-one pos label is
    "SPN", add I(O)B-labels, and print the result to stdout.
    """
    def _merge_but_drop_overlaps(senior_dict, junior_dict):
        """
        Merge ranges but in case of conflicting ranges give precedence to the
        earlier defined one.

        senior_dict is merged 'in-place'
        
        Arguments:
            senior_dict     -- dict of 2-tuple keys (rng_start, rng_end), and
                               value (the label) 
            junior_dict     -- identical to the former one


        Notes on conflicting ranges:
            conflicts are: identical, and overlapping ranges
                +---+ +---+    +------+
                +---+  +----+ +----+
            allowed are: (proper) sub-, and non-overlapping -ranges
                +---+ +--+  +---+
                +--+  +---+      +---+
        """
        for (j_start, j_end), j_label in junior_dict.items():
            for (s_start, s_end), _ in senior_dict.items():
                if (j_start == s_start and j_end == s_end):
                    j_label = ""
                    break
                elif (j_start < s_start and s_start < j_end and j_end < s_end):
                    j_label = ""
                    break
                elif (j_start > s_start and s_start > j_end and j_end > s_end):
                    j_label = ""
                    break
            if j_label:
                senior_dict[(j_start, j_end)] = j_label
            
    for comment, content in tp_blocks(stream):
        lemmata = get_content_col(content, get_fields_col_number(comment,
            "lemma")) 
        pos_tags = get_content_col(content, get_fields_col_number(comment,
            "pos"))
        
        col_lines_list = ["" for idx in range(len(content))]
        col_lines_jdict = {}

        # check the current tp block against all patterns in the all files
        for pattern_id in range(len(patterns_lists)):
            label, patterns = patterns_lists[pattern_id]
            local_col_lines_dict = {}
            for matches in find_patterns(patterns, lemmata):
                start_pos, length = matches[0], matches[1] 
                if find_patterns([["SPN"]], 
                        pos_tags[start_pos:start_pos+length]):
                    local_col_lines_dict[(start_pos, start_pos+length-1)] = (
                            label)
            # merge latest matches with earlier ones (but resolve problems)
            _merge_but_drop_overlaps(col_lines_jdict, local_col_lines_dict)
        # prepare the new nerType column, and set it
        for (start, end), label in col_lines_jdict.items():
            for idx in range(1+end - start):
                if idx == 0:
                    val = "B-"+label
                else:
                    val = "I-"+label
                col_lines_list[start+idx] = val
        content = add_content_col(content, col_lines_list)

        print "\n".join(add_fields_col(comment, "nerType"))
        print "\n".join(content)
        print

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

# with no sigle label:file mapping don't bother doing anything
if len(argv) < 2:
    print >> stderr, usage() 
    exit(1)

# split the label:file mapping into its parts, and read the content
LABELS_DICT = dict(arg.split(":", 1) for arg in argv[1:])
PATTERNS_LISTS = []
for LABEL, LABEL_FILE_NAME in LABELS_DICT.items():
    LABEL_FILE = open(LABEL_FILE_NAME,'r')
    PATTERNS = []
    while LABEL_FILE:
        LINE = LABEL_FILE.readline()
        if LINE == "":
            break
        elif LINE.startswith("#"):
            pass
        else:
            PATTERNS.append(LINE.strip().split())
    PATTERNS_LISTS.append((LABEL, PATTERNS))

gazetteer(PATTERNS_LISTS, stdin)
