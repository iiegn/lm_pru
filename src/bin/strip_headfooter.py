#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s FILE FILE...

    FILE        -- text files; each is considered an individual page. 
    
post-process txt of single pages from FILEs to standard output: try to strip
header and footer.  If similar lines occur more than twice, either within the
header or the footer, these lines will be deleted.
""" 

from os.path import basename
from sys import argv, exit, stderr
import re

def dedigit(line_str):
    """
    Substitute digits in line_str; e.g. to recognize (changing) page numbers
    """
    return re.sub(r"\d", "#", line_str)

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

# with no sigle label:file mapping don't bother doing anything
if len(argv) < 3:
    print >> stderr, usage() 
    exit(1)

CHK_LINES = 10  #: number of lines to consider at top and bottom of page
MIN_OCCS = 2    #: a lines must occur at least this number of times

pages = []      #: list of: lists of lines, the pages
header = {}     #: dict of: the top CHK_LINES of each page
footer = {}     #: dict of: the bottom CHK_LINES of each page

# fill 'list of pages' with files from command line
for pfile in argv[1:]:
    page = [line.strip() for line in open(pfile)]
    pages.append(page)

# fill dicts for header and footer with content to check against later
for page in pages:
    for line in page[0:CHK_LINES]:
        line = dedigit(line)
        header[line] = header.get(line, 0) + 1

    for line in page[-CHK_LINES:]:
        line = dedigit(line)
        footer[line] = footer.get(line, 0) + 1

# check all beginnings and endings of pages against dicts
# (though, leave out header of first page: otherwise, the title might get
# stripped)
for pnum, page in enumerate(pages):
    mypage = ""
    for lnum, line in enumerate(page):
        if ((pnum > 0 and lnum <= CHK_LINES) or 
                (lnum >= len(page)-CHK_LINES)):
            myline = dedigit(line)
            if myline != "" and (header.get(myline, 0) > MIN_OCCS  or 
                    footer.get(myline, 0) > MIN_OCCS):
                pass
            else:
                mypage = "\n".join([mypage, line])
        else:
            mypage = "\n".join([mypage, line])
    mypage = re.sub(r"^\s+", "", mypage)
    mypage = re.sub(r"\s+$", "", mypage)
    print mypage.strip()
    print
