#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: %s COL_FILE:COL_LABEL [COL_NUMBER] [--sync iel,...] < TP_file

    COL_FILE:COL_LABEL      -- COL_FILE is the file containing the new
                                column data
                               
                               COL_LABEL will be the label given to the column

    COL_NUMBER              -- the column number to overwrite (optional - 
                                default: find col COL_LABEL or add at the end)

    --sync                  -- use a specific sync strategy:
                                iel: ignore-empty-lines in COL_FILE and use
                                     them one-to-one

    TextPro_file            -- TextPro file


post-process textpro file: add column information from another file.
"""

from os.path import basename
from sys import argv, exit, stderr, stdin

from lib_textpro import (add_col, tp_blocks)

def add_column(col_stream, col_label, tpblocks_stream, col_num=None,
        strategy=None):
    """
    Set or add column col_label/col_num in col_stream, and
    print the altered comment and content.

    Arguments:
        col_stream  -- the input stream of the new column content
        tpblocks_stream -- a 'TextPro file' input stream (e.g. sys.stdin)
        col_label   -- string: name of the column

        col_num     -- int of the column to set 
        strategy    -- iel:ignore-empty-lines in col_stream    
    """
    def _read_num_lines(num, lines_stream, strategy=None):
        """Hepler function to read the new column content."""
        lines_list = []
        while len(lines_list) < num:
            line = lines_stream.readline()
            if line == "\n" and len(lines_list) == 0:
                pass
            elif line == "\n" and strategy == "iel":
                pass
            elif line == "\n" and len(lines_list) > 0:
                lines_list.extend(["" for _idx in range(num - len(lines_list))])
            else:
                lines_list.append(line.strip())
        return lines_list

    for comment, content in tp_blocks(tpblocks_stream):
        col_lines_list = _read_num_lines(len(content), col_stream,
                strategy=strategy)
        
        comment, content = add_col(comment, content, col_label, col_lines_list,
                col_num=col_num) 
        print "\n".join(comment)
        print "\n".join(content)
        print

def usage():
    """ Return help message."""
    return __doc__ % (basename(argv[0]))

# with no single file:label mapping don't bother doing anything
if len(argv) < 2 or len(argv) > 5:
    print >> stderr, usage() 
    exit(1)

# split the file:label mapping into its parts
COL_FILE, COL_LABEL = argv[1].split(":", 1)

COL_NUMBER = None
SYNC_STRATEGY = None
if len(argv) > 3:
    argv_idx = 2
    if not argv[2].startswith("--sync"):
        COL_NUMBER = int(argv[2])
        argv_idx += 1
    if argv[argv_idx].startswith("--sync"):
        SYNC_STRATEGY = argv[argv_idx+1]

add_column(open(COL_FILE,'r'), COL_LABEL, stdin,
        col_num=COL_NUMBER, strategy=SYNC_STRATEGY)
