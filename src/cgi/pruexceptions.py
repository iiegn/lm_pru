# -*- coding: iso-8859-1 -*-
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class DBError(Error):
    """Exception raised for Error during DB init.

    Attributes:
        msg  -- explanation of the error
    """

    def __init__(self, msg):
        self.msg = "".join([self.__class__.__name__,":",msg])

class TransitionError(Error):
    """Raised when an operation attempts a state transition that's not
    allowed.

    Attributes:
        prev -- state at beginning of transition
        next -- attempted new state
        msg  -- explanation of why the specific transition is not allowed
    """

    def __init__(self, prev, next, msg):
        self.prev = prev
        self.next = next
        self.msg = msg
