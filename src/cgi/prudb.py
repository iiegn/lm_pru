# -*- coding: iso-8859-1 -*-
"""
DB abstraction for the PRU RESTful service.
"""

import os
import shutil

import uuid

import pru
from pruexceptions import DBError

DBDIR = '/mnt/data2/egon/pru.db'
INDIR_SRC_NAME = 'doc.src'
INDIR_STS_NAME = 'status'
INDIR_LOG_NAME = 'log'
INDIR_DOC_NAME = 'doc.out'


def initdb():
    """
    initialise the DB; do nothing if DB exists.
    """
    if os.access(DBDIR, os.F_OK):
        pass
    else:
        raise DBError("""directory %s does not exists.""" % (DBDIR))

def reinitdb():
    """
    flush all contents in the DB and then, initialise DB.
    """
    _deldb()
    initdb()

def del_document(doc_id):
    """
    delete a document.

    Attributes:
        doc_id -- document ID to be deleted
    """
    try:
        shutil.rmtree(os.path.join(DBDIR, doc_id))
    except OSError, err:
        raise DBError("""could not delete document with ID %s.""" % (doc_id))

def add_document(doc, lang="auto"):
    """
    add a document to the DB and return its ID.

    Attributes:
        doc  -- content of the document
        lang -- string represneting the ISO code of the content's language
                "auto" for autodetection
    """
    id = str(uuid.uuid4())
    try:
        from os import environ, umask
        umask(0000)
        os.mkdir(os.path.join(DBDIR, id))
        _tmp = open(os.path.join(DBDIR, id, INDIR_SRC_NAME), 'wb')
        _tmp.write(doc)
        _tmp.close()
        open(os.path.join(DBDIR, id, "uploaded"), 'w').close()
        _tmp = open(os.path.join(DBDIR, id, INDIR_STS_NAME), 'w')
        _tmp.write("Q")
        _tmp.close()
    except OSError, err:
        try:
            _tmp = open(os.path.join(DBDIR, id, INDIR_STS_NAME), 'w')
            _tmp.write("F")
            _tmp.close()
        except:
            pass
        raise DBError("""could not add document to DB.""")
    return id

def get_pdf(doc_id):
    return open(get_src_name(doc_id), 'r')

def get_status(doc_id):
    fh = open(_get_status_name(doc_id), 'r')
    status = fh.readline().strip()
    fh.close()
    return status 

def get_zip(doc_id):
    return open(get_zip_name(doc_id), 'rb')

def get_text(filename):
    txt = []
    fh = open(filename, 'r')
    while fh:
        line = fh.readline()
        if line:
            txt.append(line)
        else:
            break
    fh.close()
    return "".join(txt)

def get_log(doc_id):
    filename = os.path.join(get_dir_name(doc_id), INDIR_LOG_NAME)
    return get_text(filename)

def get_any_text(anyfile, doc_id):
    filename = os.path.join(get_dir_name(doc_id), INDIR_SRC_NAME + anyfile)
    return get_text(filename)

def get_dir_name(doc_id):
    return os.path.join(DBDIR, doc_id)

def get_src_name(doc_id):
    return os.path.join(get_dir_name(doc_id), INDIR_SRC_NAME)

def get_doc_name(doc_id):
    return os.path.join(get_dir_name(doc_id), INDIR_DOC_NAME)

def _get_status_name(doc_id):
    return os.path.join(get_dir_name(doc_id), INDIR_STS_NAME)

def get_zip_name(doc_id):
    return ".".join([get_dir_name(doc_id), "tar.gz"])

def replace_document(doc_id, doc):
    """
    replace document with another one.
    """
    # FIXME: implement
    pass

def _get_document_ids():
    """
    return a list with all document IDs.
    """
    id_list = []
    for file in os.listdir(DBDIR):
        if os.path.isdir(os.path.join(DBDIR, file)):
            id_list.append(file)
    return id_list

def _deldb():
    for id in _get_document_ids():
        try:
            del_document(id)
        except DBError, err:
            pru.print_err(err.msg)
