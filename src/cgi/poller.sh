#!/usr/bin/env bash
#
# backend: watch out for new file uploads, and run the pipeline on all

dir_to_monitor="/mnt/data2/egon/pru.db"
stat_old="/tmp/pru.o$$"
stat_cur="/tmp/pru.c$$"

find_args="-mindepth 1 -maxdepth 1 -type d"

if [ ! -e $stat_old ]
then
    find "$dir_to_monitor" $find_args | sort > $stat_old
fi

while true
do
    find "$dir_to_monitor" $find_args | sort > $stat_cur
    for new_dir in $(diff $stat_old $stat_cur | grep -e "^>" | cut -d' ' -f2)
    do
        sleepy=1
        while [ ! -e ${new_dir}/uploaded ]
        do
            sleep 5
            sleepy=$(($sleepy + 1))
            if [ $sleepy -gt 10 ]; then break; fi
        done
        
        # this duplicates stdout and stderr to a log file
        #exec 1> >(tee -a ${new_dir}/log)
        #exec 2> >(tee -a ${new_dir}/log)

        date | tee -a ${new_dir}/log
        echo "R" > $new_dir/status
        $(dirname $0)/../bin/pipeline.sh $new_dir/doc.src \
        1> >(tee -a ${new_dir}/log) 2> >(tee -a ${new_dir}/log) \
        && echo "S" > $new_dir/status \
        || echo "F" > $new_dir/status
        date | tee -a ${new_dir}/log

    done
    sleep 10 
    mv $stat_cur $stat_old
done
