#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
"""
Main handler of RESTful interaction.
"""
import cgi
import cgitb; cgitb.enable()
import os
import subprocess
import sys

import pru
import prudb

message = ""
header_html = """Content-type: text/html; charset="UTF-8"\n
<html>
<head><title>LM/PRU Pipeline</title></head>
<body>"""
footer_html = """</body></html>"""

header_text_lat1 = """Content-type: text/plain; charset="latin-1"\n\n"""
header_text_utf8 = """Content-type: text/plain; charset="utf-8"\n\n"""
header_text = header_text_lat1 
footer_html = ""

header_xml = """Content-type: text/xml\n
<?xml version="1.0" encoding="UTF-8" ?>
"""
footer_xml = ""

header = header_html
footer = footer_html
message = ""

if pru.http_action == "POST":
    header = header_xml + """
    <uploadResponse>
    """
    footer = """
    </uploadResponse>
    """ + footer_xml

    form = cgi.FieldStorage()
    # Get filename here.
    filedata = form.getfirst("upfile")
    filename = form.getfirst("filename", "unknown.pdf")

    # Test if the file was uploaded
    if filedata and filename:
        id = prudb.add_document(filedata)
        
        message += """<base_url>./documents.cgi/%s/</base_url>""" %(id)
        message += """<id>%s</id>""" %(id)
        message += """<status>./documents.cgi/%s/status</status>""" %(id)
        message += """<log>./documents.cgi/%s/log</log>""" %(id)
        message += """<bib>./documents.cgi/%s/bib</bib>""" %(id)
        message += """<pretxp>./documents.cgi/%s/pretxp</pretxp>""" %(id)
        message += """<txp>./documents.cgi/%s/txp</txp>""" %(id)
        message += """<zip>./documents.cgi/%s/zip</zip>""" %(id)
    else:
        message += 'Sorry, an error occurred during upload:'
        for key in form:
            message += "<b>%20s</b>: %s<br>" % (key,form[key])

elif pru.http_action == "GET":
    header = header_html
    if pru.doc_id:
        if pru.doc_more == "status":
            header = header_xml
            footer = footer_xml
            message += "<status>" + prudb.get_status(pru.doc_id) + "</status>"
        elif pru.doc_more == "pdf":
            out = os.fdopen(sys.stdout.fileno(), 'wb')
            out.write("""Content-Type:application/pdf; name="%s.pdf"\n""" 
                    %(pru.doc_id))
            out.write("""Content-Disposition: attachment; filename="%s.pdf"\n\n""" 
                    %(pru.doc_id))
            pdf = prudb.get_pdf(pru.doc_id)
            out.write(pdf.read())
            out.flush()
            out.close()
            sys.exit(0)
        elif pru.doc_more == "log":
            header = header_text
            footer = ""
            message = prudb.get_log(pru.doc_id)
        elif pru.doc_more == "bib":
            header = header_text
            footer = ""
            message = prudb.get_any_text(".xml.bib", pru.doc_id)
        elif pru.doc_more == "pretxp":
            header = header_text
            footer = ""
            message = prudb.get_any_text(".pretxp", pru.doc_id)
        elif pru.doc_more == "txp":
            header = header_text_utf8
            footer = ""
            message = prudb.get_any_text(".txp.chn.gaz", pru.doc_id)
        elif pru.doc_more == "zip":
            out = os.fdopen(sys.stdout.fileno(), 'wb')
            out.write("""Content-Type:application/x-gzip; name="%s.tar.gz"\n""" 
                    %(pru.doc_id))
            out.write("""Content-Disposition: attachment; filename="%s.tar.gz"\n\n""" 
                    %(pru.doc_id))
            zippy = prudb.get_zip(pru.doc_id)
            out.write(zippy.read())
            out.flush()
            out.close()
            sys.exit(0)
        else:
            message += "GET: %s,%s" %(pru.doc_id,pru.doc_more)
    else:
        message += "GET"

elif pru.http_action == "DELETE":
    header = header_html
    if pru.doc_id:
        prudb.del_document(pru.doc_id)
        message = 'deleted document %s' % (pru.doc_id)
    else:
        prudb.reinitdb()
        message += 'deleted all documents'

out = os.fdopen(sys.stdout.fileno(), 'wb')
out.write("%s\n" % (header))
out.write("%s" % (message))
out.write("%s\n" % (footer))
out.close()
sys.exit(0)
