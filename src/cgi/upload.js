// don't cache results
$.ajaxSetup ({  
    cache: false  
}) 

// wait for the DOM to be loaded 
$(document).ready(function()
{ 
    // bind 'myForm' and provide a simple callback function 
    $('#htmlForm').ajaxForm(
    { 
        target: '#formResponseTarget', 
        dataType: 'xml',
        beforeSubmit: function() {
            $('div#uploadResponse').fadeIn();
        },
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) {

            function ajax_poll_backend_status() {
                $.ajax({
                    type : "GET",
                    url : status_url,
                    dataType : "xml",
                    success : function(data)
                    {
                        $('div#processingTarget').fadeIn(1000);

                        // debug output
                        console.log(data);

                        // show status 
                        var backend_status = $("status", data).text();
                        $('div#processingStatusTarget').html(backend_status);

                        // job is queued - keep polling...
                        if (backend_status == "Q")
                        { 
                            setTimeout(function() 
                                { 
                                    ajax_poll_backend_status(); 
                                }, 
                            5000);
                        }
                        // job is running - update the log view, and keep polling...
                        else if (backend_status == "R")
                        {
                            $('div#processingLogTarget').load(log_url).fadeIn(1000);
                            setTimeout(function() 
                                { 
                                    ajax_poll_backend_status(); 
                                }, 
                            2000);
                        }
                        // job finished - show results
                        else if (backend_status == "S")
                        {
                            // add links for the individual results
                            $('div#formResponseTarget').append(
                            a_tag("bib", base_url + "bib")  + 
                            a_tag("pretxp", base_url + "pretxp") + 
                            a_tag("txp", base_url + "txp") +
                            a_tag("zip", base_url + "zip") +
                            a_tag("pdf", base_url + "pdf") ).fadeIn(1000);

                            // exchange log view with actual results
                            $('div#processingLogTarget').load(log_url).fadeIn(1000);
                            $('div#processedTarget').fadeIn(1000);
                            $('div#processingTarget').fadeOut(2500);

                            // populate content
                            var tmp_node = $("<pre></pre>").load(base_url + "pretxp")
                            $('div#pretxp').html(tmp_node).fadeIn(1000);
                            var tmp_node = $("<pre></pre>").load(base_url + "bib")
                            $('div#bib').html(tmp_node).fadeIn(1000);
                        }
                        // job ended with failure
                        else
                        {
                            // assume: backend_status == "F"
                        }
                    }
                });
            }

            console.log(responseText);
            var id = $("id", responseText).text();
            var base_url = $("base_url", responseText).text();
            var status_url = $("status", responseText).text();
            var log_url = $("log", responseText).text();

            function a_tag(link_name, link_url)
            {
                var a_tag_pre = "<a target=\"_blank\" href=\"";
                var a_tag_post = "\">" + link_name + "</a> ";
                return a_tag_pre + link_url + a_tag_post;
            }

            if (id)
            {
                $('div#formResponseTarget').html(
                "ID: " + id + "<br />" +
                "BASE_URL: " + base_url + "<br />" +
                a_tag("status", status_url) +
                a_tag("log", log_url) );

                ajax_poll_backend_status();

            } else {
                $('div#formResponseTarget').text(responseText);
            }
        } 
    }); 
})
