#/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import codecs
import sys
sys.stdout = codecs.getwriter('utf8')(sys.stdout)
from os import environ

import prudb

http_action = environ.get("REQUEST_METHOD")
try:
    doc_id, doc_more = environ.get("PATH_INFO", "/").lstrip("/").split("/", 1)
except ValueError:
    doc_id = environ.get("PATH_INFO", "/").lstrip("/")

def print_err(msg):
    print >> sys.stderr, msg

def exit_err(msg):
    print_err(msg)
    sys.exit(1)

if __name__ == '__main__':
    try:
        prudb.reinitdb()
    except DBError, err:
        exit_err(err.msg)
